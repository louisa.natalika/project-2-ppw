[![pipeline status](https://gitlab.com/louisa.natalika/project-2-ppw/badges/master/pipeline.svg)](https://gitlab.com/louisa.natalika/project-2-ppw/commits/master)
[![pipeline status](https://gitlab.com/louisa.natalika/project-2-ppw/badges/master/coverage.svg)](https://gitlab.com/louisa.natalika/project-2-ppw/commits/master)

# About
This page allows visitors to input about their status and show the input immediately

## Heroku
[lika-project2.herokuapp.com](lika-project2.herokuapp.com)

