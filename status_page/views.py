from django.shortcuts import render
from .forms import StatusForm
from .models import Status

# Create your views here.

def status(request):
    if request.method == "POST":
        form = StatusForm(request.POST)
        if form.is_valid:
            form.save()
    form = StatusForm()
    all_status = Status.objects.all()
    arguments = {
        'form': form,
        'all_status': all_status,
    }
    return render(request, 'status_page.html', arguments) 