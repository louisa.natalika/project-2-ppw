from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from django.core.exceptions import ValidationError
from datetime import datetime, timedelta
from unittest import mock
from .views import status
from .models import Status
from .forms import StatusForm

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from selenium.webdriver.common.keys import Keys


# Create your tests here.

class UnitTestStatusPage(TestCase):
    def test_apakah_ada_url_ada(self):
        c = Client()
        response = c.get('/')
        self.assertEqual(response.status_code, 200)

    def test_apakah_url_kosong_menggunakan_fungsi_index(self):
        found = resolve('/')
        self.assertEqual(found.func, status)
    
    def test_apakah_data_status_berhasil_dimasukkan_ke_dalam_database(self):
        status =  Status.objects.create(status='sibuk')
        seluruh_status = Status.objects.all()
        self.assertEqual(str(status), 'sibuk')
        self.assertEqual(seluruh_status.count(), 1)
        self.assertEqual(Status.objects.get(id=1).status, 'sibuk')

    def test_apakah_waktu_ditampilkan_sesuai_dengan_waktu_status_dibuat(self):
        status = Status.objects.create(status='lapar')
        time = Status.objects.get(id=1).time + timedelta(hours=7)
        self.assertEqual(time.strftime("%m-%d-%Y %H:%M:%S"),datetime.now().strftime("%m-%d-%Y %H:%M:%S"))
       
    def test_apakah_form_valid_dapat_dibuat_dan_disimpan(self):
        data_form = {
            'status': 'cape'
        }
        form = StatusForm(data=data_form)
        self.assertTrue(form.is_valid())
        form.save()
        self.assertEqual(Status.objects.all().count(), 1)
        self.assertEqual(Status.objects.get(id=1).status, 'cape')
    
    def test_form_tidak_valid_tidak_dapat_dibuat_dan_disimpan(self):
        form = StatusForm({})
        self.assertFalse(form.is_valid())
        self.assertEqual(Status.objects.all().count(), 0)
       
    def test_apakah_fungsi_status_bekerja_saat_mendapat_request_GET(self):
        c = Client()
        response = c.get('/')
        content = response.content.decode('utf-8')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('status_page.html')
        self.assertIn("<form", content)
        self.assertIn('<input', content)
        
    def test_apakah_fungsi_status_bekerja_saat_mendapat_POST_dengan_input_valid(self):
        c = Client()
        response = c.post('/', {'status': 'mengantuk'})
        content = response.content.decode('utf-8')
        stat = Status.objects.get(id=1)
        self.assertEqual(stat.status, 'mengantuk')
        self.assertIn(stat.status, content)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('status_page.html')
        self.assertIn("<form", content)
        self.assertIn('<input', content)
    
    def test_apakah_fungsi_status_bekerja_saat_mendapat_POST_dengan_input_kosong(self):
        c = Client()
        response = c.post('/', {'status': None})
        content = response.content.decode('utf-8')
        self.assertRaises(ValidationError)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('status_page.html')  
        self.assertIn("<form", content)
        self.assertIn('<input', content)

class FunctionalTestStatusPage(StaticLiveServerTestCase):

    def setUp(self):
        options = Options()
        options.add_argument('--headless')
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        #membuat instance dari chrome webdriver
        self.browser = webdriver.Chrome(chrome_options=options)
    
    def tearDown(self):
        self.browser.close()

    def test_title_sesuai(self):
        self.browser.get(self.live_server_url)
        self.assertIn("Status Lika", self.browser.title)
    
    def test_apakah_status_berhasil_dimasukkan(self):
        self.browser.get(self.live_server_url)
        form = self.browser.find_element_by_name("status")
        status_input = "mengantuk"
        form.send_keys(status_input)
        button = self.browser.find_element_by_id("submit-button")
        button.send_keys(Keys.RETURN)
        self.assertIn(status_input, self.browser.page_source)