from django.urls import path
from . import views

app_name = 'status_page'

urlpatterns = [
    path('', views.status, name='status'),
]